# Social Tech

Social Tech is a school project made to solve a real-world problem from one of the participating companies. The best solution would then be used by the business.
<br>
<br>
We developed a WebApp for SMILE Business Solutions, the webapp was made to streamline their business and help improve the way they worked with their clients.
<br>
<br>
SMILE is a life coaching company that helps other SME Businesses and individuals improve their lives by identifying problems and setting goals to alleviate these issues.
The coach would then meet on a regular basis to check up on the client and record the progress made towards their goals and offer advice and suggestions along the way.
<br>
<br>
Our WebApp got rid of their need for pen and paper and the issues of scheduling in-person meetings (which were oftern cancelled), as they could now do everything online in one place.
The coach would be able to check on their progress and message the client.
<br>
<br>
We attempted to make a friendly themed site and decided going with a natured themed site
<br>



# Project Overview

There are 3 main parts this project was split up into:

Database    - PostgreSQL
API         - ExpressJS 
WebApp      - Angular

### I did the work on WebApp part while my other 2 team-mates worked on the database and API respectavly

<b>The project is also [Hosted on Heroku!](https://smile-front-end.herokuapp.com)</b>
<br>Check it out

<b>User:       smile@dev-api.ru</b>
<br>
<b>Password:   password</b>

# Web Preview


![](https://gitlab.com/Heagan/socialtech/raw/master/preview/Login.PNG)
![](https://gitlab.com/Heagan/socialtech/raw/master/preview/Home.PNG)
![](https://gitlab.com/Heagan/socialtech/raw/master/preview/NoteBig.PNG)

## Also made to work on mobile devices

![](https://gitlab.com/Heagan/socialtech/raw/master/preview/Goals.PNG)
![](https://gitlab.com/Heagan/socialtech/raw/master/preview/NoteSmall.PNG)
![](https://gitlab.com/Heagan/socialtech/raw/master/preview/Slide.PNG)

## Also created a fully functioning dashboard for administrators

![](https://gitlab.com/Heagan/socialtech/raw/master/preview/Admin.PNG)

